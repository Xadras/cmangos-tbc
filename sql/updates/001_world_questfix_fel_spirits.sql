-- Update NPCs to use Script
UPDATE `creature_template` SET `ScriptName`='npc_anchorite_relic_bunny' WHERE (`Entry`='22444');
UPDATE `creature_template` SET `AIName`='', `ScriptName`='npc_hand_berserker' WHERE (`Entry`='16878');
-- Fel spirit should NOT drop anything
UPDATE `creature_template` SET `LootId`='0' WHERE (`Entry`='22454');
-- Relict should NOT be targetable/clickable
UPDATE `gameobject_template` SET `flags`='4' WHERE (`entry`='185298');